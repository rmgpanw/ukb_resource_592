# Clinical coding classification systems and maps

An online version of [UKB resource 592](https://biobank.ndph.ox.ac.uk/ukb/refer.cgi?id=592) for convenient browsing.

To set up:

1. Download [UKB resource 592](https://biobank.ndph.ox.ac.uk/ukb/refer.cgi?id=592), unzip and update the path to `all_lkps_maps_v3.xlsx` in `.Renviron`. 
2. Install required packages using `devtools::install_github("rmgpanw/codemapper")` then `renv::restore()`
3. Run targets pipeline with `targets::tar_make()`
